const fetch = require('node-fetch')
const url = process.env.S3URL
const branch = process.env.BRANCH
const slackSecret = process.env.SLACK_SECRET

var message = {
  'channel': '@mhume',
  'username': 'hexo',
  'text': `>>>Branch \`${branch}\` ready for review at:\r${url}/review/${branch}`,
  'icon_emoji': ':beryl:'
}

fetch(`https://hooks.slack.com/services/${slackSecret}`, { method: 'POST', body: JSON.stringify(message) })
