#!/usr/bin/env bash
latest=$(git log -1 --pretty=%B)
json="{
\"channel\": \"#ua-bootstrap\",
\"username\":\"hexo\",
\"icon_emoji\":\":beryl:\",
\"attachments\":[{
    \"fallback\": \"Visual Diff ready for review at: ${S3URL}/review/${BRANCH}/backstop_data/html_report/index.html\",
    \"color\":\"#FF6347\",
    \"author_name\": \"uadigital/ua-bootstrap\",
    \"author_link\": \"https://bitbucket.org/uadigital/ua-bootstrap\",
    \"title\": \"Click here to view what has changed with BackstopJS. \",
    \"title_link\": \"${S3URL}/review/${BRANCH}/backstop_data/html_report/index.html \",
    \"text\": \"$latest\"
    }]
}"

curl -s -d "payload=$json" "https://hooks.slack.com/services/${SLACK_SECRET}"

