
<h1 id="alerts">Alerts</h1>

<p class="lead">Provide contextual feedback messages for typical user actions with the handful of available and flexible alert messages.</p>

<h3 id="alerts-examples">Examples</h3>
<p>Wrap any text and an optional dismiss button in <code>.alert</code> and one of the four contextual classes (e.g., <code>.alert-success</code>) for basic alert messages.</p>

<div class="bs-callout bs-callout-info" id="callout-alerts-no-default">
  <h4>No default class</h4>
  <p>Alerts don't have default classes, only base and modifier classes. A default gray alert doesn't make too much sense, so you're required to specify a type via contextual class. Choose from success, info, warning, or danger.</p> 
  
  <p>These alert messages are suggestions, but feel free to use them. </p>
</div>

<div class="example" data-example-id="simple-alerts">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="alert alert-success" role="alert">
    <strong>Excellent!</strong> You&apos;ve successfully read this important alert message.
  </div>
  <div class="alert alert-info" role="alert">
    <strong>Alert!</strong> This issue needs your attention, but it&apos;s not urgent.
  </div>
  <div class="alert alert-warning" role="alert">
    <strong>Attention!</strong> This alert needs your consideration before you proceed.
  </div>
  <div class="alert alert-danger" role="alert">
    <strong>Error!</strong> Change a few things and try submitting again.
  </div>
</div>

```html
<div class="alert alert-success" role="alert">...</div>
<div class="alert alert-info" role="alert">...</div>
<div class="alert alert-warning" role="alert">...</div>
<div class="alert alert-danger" role="alert">...</div>
```
<h3 id="alerts-dismissible">Dismissible alerts</h3>
<p>Build on any alert by adding an optional <code>.alert-dismissible</code> and close button.</p>
<div class="bs-callout bs-callout-info" id="callout-alerts-dismiss-plugin">
  <h4>Requires JavaScript alert plugin</h4>
  <p>For fully functioning, dismissible alerts, you must use the <a href="../javascript.html#alerts">alerts JavaScript plugin</a>.</p>
</div>
<div class="example" data-example-id="dismissible-alert-css">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="alert alert-warning alert-dismissible" role="alert">
    <button data-dismiss="alert" type="button" class="close"><i class="ua-brand-x"></i></button>
    <strong>Attention!</strong> This alert needs your consideration before you proceed.
  </div>
</div>

```html
<div class="alert alert-warning alert-dismissible" role="alert">
<button data-dismiss="alert" type="button" class="close"><i class="ua-brand-x"></i></button>
<strong>Warning!</strong> Better check yourself, you&apos;re not looking too good.
</div>
```

<div class="bs-callout bs-callout-warning" id="callout-alerts-dismiss-use-button">
  <h4>Ensure proper behavior across all devices</h4>
  <p>Be sure to use the <code>&lt;button&gt;</code> element with the <code>data-dismiss="alert"</code> data attribute.</p>
</div>

<h3 id="alerts-links">Links in alerts</h3>
<p>Use the <code>.alert-link</code> utility class to quickly provide matching colored links within any alert.</p>
<div class="example" data-example-id="alerts-with-links">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="alert alert-success" role="alert">
    <strong>Excellent!</strong> You&apos;ve successfully read <a href="#" class="alert-link">this important alert message</a>.
  </div>
  <div class="alert alert-info" role="alert">
    <strong>Alert!</strong> This <a href="#" class="alert-link">issue needs your attention</a>, but it&apos;s not urgent.
  </div>
  <div class="alert alert-warning" role="alert">
    <strong>Attention!</strong> This alert needs your <a href="#" class="alert-link">consideration</a> before you proceed.
  </div>
  <div class="alert alert-danger" role="alert">
    <strong>Error!</strong> <a href="#" class="alert-link">Change a few things</a> and try submitting again.
  </div>
</div>

```html
<div class="alert alert-success" role="alert">
<a href="#" class="alert-link">...</a>
</div>
<div class="alert alert-info" role="alert">
<a href="#" class="alert-link">...</a>
</div>
<div class="alert alert-warning" role="alert">
<a href="#" class="alert-link">...</a>
</div>
<div class="alert alert-danger" role="alert">
<a href="#" class="alert-link">...</a>
</div>
```


